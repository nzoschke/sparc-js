var passport = require('koa-passport')

var user = { id: 1, username: 'test' }

passport.serializeUser(function(user, done) {
  done(null, user.id)
})

passport.deserializeUser(function(id, done) {
  done(null, user)
})

var GitHubStrategy = require('passport-github').Strategy
passport.use(new GitHubStrategy({
    clientID: 'c6582806e7a35003425f',
    clientSecret: '13bb219eca82ec3298b7809c957e46da5e4ee7b7',
    callbackURL: 'http://127.0.0.1:' + (process.env.PORT || 3000) + '/auth/github/callback'
  },
  function(token, tokenSecret, profile, done) {
    console.log(profile)
    // retrieve user ...
    done(null, profile)
  }
))
